# lambda_function.py
import json
import urllib.parse

import urllib3

import respond, content

http = None

def lambda_handler(event, context):
    
    global http 
    
    req_method = event['httpMethod']
    req_query  = event['queryStringParameters']
    req_path   = (urllib.parse.unquote(event['path']).split('/'))[2:]

    ps = None
    am = None
    
    if http is None:
        http = urllib3.PoolManager()
        
    host = event['headers']['Host']
    stage = event['requestContext']['stage']
    
    api_url = f'https://{host}/{stage}/cumberland'
    
    # This is used so the root ps list works even if the trailing slash is omitted.
    content_path = f'/{stage}/cumberland-html'

    if req_path[-1:] == ['']:
        print('[lambda_function.py] Removing trailing slash from', event['path'])
        req_path.pop()

    if len(req_path) == 0:
        return content.index(http, api_url, req_method, ps, am, content_path)

    elif len(req_path) == 1:
        
        if req_path[0] == 'diag':
            return diag(event, api_url, stage)

        ps = req_path[0].replace('+', ' ').replace('_', ' ')
        return content.planning_scheme(http, api_url, req_method, ps, am)

    elif len(req_path) == 2:
        ps = req_path[0].replace('+', ' ').replace('_', ' ')
        am = req_path[1].replace('+', ' ').replace('_', ' ')
        return content.amendment(http, api_url, req_method, req_query, ps, am)

    elif (len(req_path) == 3) and (req_path[2] == 'maps'):
        ps = req_path[0].replace('+', ' ').replace('_', ' ')
        am = req_path[1].replace('+', ' ').replace('_', ' ')
        return content.amendment(http, api_url, req_method, req_query, ps, am + '/maps')

    return respond.error(code='invalid-url', 
        message='Expecting a planning scheme and amendment in the url, for example Alpine/2020-02-11 VC168', 
        status_code=404, type='URL Format Error')


def diag(event, api_url, stage):
    
    diag_data = dict(
        api_url=api_url,
        stage=stage,
        path=event['path'],
        http_method=event['httpMethod'],
        headers=event['headers'],
        )
      
    result = '<pre>' + json.dumps(diag_data, indent=2) + '</pre>'
    
    return respond.error(code='diag', 
        message=result, 
        status_code=200, type='Diagnostic Information')

